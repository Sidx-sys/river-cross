import math
import random
import sys
import os
from abc import ABC
import configparser
import pygame
from pygame import mixer
from pygame import time

configParser = configparser.RawConfigParser()
configFilePath = os.path.join(os.path.dirname(__file__), 'myConfig.cfg')
configParser.read(configFilePath)
gameName = configParser.get("info", "Name")
gameWidth = int(configParser.get("info", "Width"))
gameHeight = int(configParser.get("info", "Height"))
gameFont = configParser.get("info", "Font")
background_image = configParser.get("info", "background_image")
ships = configParser.get("info", "ships")
skulls = configParser.get("info", "skulls")
p1_img = configParser.get("info", "p1_img")
p2_img = configParser.get("info", "p2_img")
dead_animation = configParser.get("info", "dead_animation")
success = configParser.get("info", "success_msg")
hit = configParser.get("info", "hit_msg")
blue = configParser.get("info", "blue")
# converting the hex blue to rgb blue
color = blue.lstrip('#')
blue = tuple(int(color[i:i + 2], 16) for i in (0, 2, 4))

# initializing the game console
pygame.init()

# creating the screen
screen = pygame.display.set_mode((gameWidth, gameHeight))

# background music
mixer.music.load('Music/No Monkey.wav')
mixer.music.play(-1)

# loading images
background = pygame.image.load(background_image)
moving_enemy = pygame.image.load(ships)
static_enemy = pygame.image.load(skulls)
player1Img = pygame.image.load(p1_img)
player2Img = pygame.image.load(p2_img)
ghost = pygame.image.load(dead_animation)

# title
pygame.display.set_caption("River Cross")
icon = pygame.image.load('Images/icon.png')
pygame.display.set_icon(icon)

p1_score = 0
p2_score = 0
score = []
# text
text_font = pygame.font.Font(gameFont, 22)
text_font_m = pygame.font.Font(gameFont, 40)
text_font_l = pygame.font.Font(gameFont, 60)


class StdObject(ABC):
    def display(self):
        screen.blit(self.image, (self.x, self.y))


class Player(StdObject):
    def __init__(self, image, x, y, x_change, y_change):
        self.image = image
        self.x = x
        self.y = y
        self.x_change = x_change
        self.y_change = y_change
        self.active = False
        self.success = False
        self.score = 0

    def update(self):
        self.x += self.x_change
        self.y += self.y_change

    def limit(self):
        if self.x <= 0:
            self.x = 0
        elif self.x >= 436:
            self.x = 436
        if self.y <= 0:
            self.y = 0
        elif self.y >= 876:
            self.y = 876


class EnemyMoving(StdObject):

    def __init__(self, image, enemy_moving_y):
        self.image = image
        self.x = random.randint(-200, -120)  # every enemy starts behind the game screen
        self.y = enemy_moving_y  # where we initialize the enemy
        self.p1pass = False
        self.p2pass = False

    def update_location(self, change_x):
        self.x += change_x
        if self.x >= 1100:
            self.x = random.randint(-200, -120)


class EnemyStatic(StdObject):
    def __init__(self, image, y):
        self.image = image
        self.x = random.randint(0, 436)  # every enemy starts behind the game screen
        self.y = y  # where we initialize the enemy
        self.p1pass = False
        self.p2pass = False


# THe Game Manager, where the game loop is controlled
def setup():
    global p1_score, p2_score
    p1_level = 1
    p2_level = 1
    max_level = 3
    while p1_level <= max_level and p2_level <= max_level:
        p1_level, p2_level = game(p1_level, p2_level)
    score.append(p1_score)
    score.append(p2_score)
    # text to display
    global text1, text2, text3, text5
    text1 = text_font_l.render("Player 1 Wins!", True, (255, 255, 255))
    text5 = text_font_l.render("Player 2 Wins!", True, (255, 255, 255))
    text2 = text_font_m.render("Player 1: " + str(score[0]), True, (255, 255, 255))
    text3 = text_font_m.render("Player 2: " + str(score[1]), True, (255, 255, 255))
    running = True
    while running:
        screen.fill(blue)
        if p1_level == 4 and p2_level < 4:
            p1win()
        elif p2_level == 4 and p1_level < 4:
            p2win()
        elif p1_level == 4 and p1_level == 4:
            if p1_score > p2_score:
                p1win()
            else:
                p2win()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
        pygame.display.update()


def p1win():
    screen.blit(text1, (73, 224))
    screen.blit(text2, (136, 587))
    screen.blit(text3, (136, 640))


def p2win():
    screen.blit(text5, (73, 224))
    screen.blit(text2, (136, 587))
    screen.blit(text3, (136, 640))


def show_enemy_moving(obj, speed):
    for enemy in obj:
        enemy.update_location(speed)
        enemy.display()


def show_enemy_static(obj):
    for enemy in obj:
        enemy.display()


def show_ghost(obj):
    screen.blit(ghost, (obj.x, obj.y))
    pygame.time.delay(250)


def score1():
    score_1 = text_font.render("Player1: " + str(p1_score), True, (255, 255, 255))
    screen.blit(score_1, (0, 0))


def score2():
    score_2 = text_font.render("Player2: " + str(p2_score), True, (255, 255, 255))
    screen.blit(score_2, (0, 0))


def is_collision(obj1, obj2):
    px = obj1.x
    py = obj1.y
    ex = obj2.x
    ey = obj2.y
    distance = math.sqrt(math.pow(ex - px, 2) + math.pow(ey - py, 2))
    if distance <= 50:
        return True


def game(p1_level, p2_level):
    global p1_score, p2_score
    moving_enemies = [EnemyMoving(moving_enemy, random.randint(83, 85)),
                      EnemyMoving(moving_enemy, random.randint(560, 600)),
                      EnemyMoving(moving_enemy, random.randint(386, 410)),
                      EnemyMoving(moving_enemy, random.randint(225, 242)),
                      EnemyMoving(moving_enemy, random.randint(755, 815))]

    static_enemies = []
    y_list = [167, 318, 490, 680]
    for i in range(14):
        static_enemies.append(EnemyStatic(static_enemy, random.choice(y_list)))

    player1 = Player(player1Img, 186, 876, 0, 0)
    player2 = Player(player2Img, 186, 0, 0, 0)

    player1.active = True
    player2.active = False

    start_time = pygame.time.get_ticks() // 1000
    running = True
    while running:
        screen.blit(background, (0, 0))
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            if player1.active:
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_LEFT:
                        player1.x_change = -4
                    if event.key == pygame.K_RIGHT:
                        player1.x_change = 4
                    if event.key == pygame.K_UP:
                        player1.y_change = -4
                    if event.key == pygame.K_DOWN:
                        player1.y_change = 4
                if event.type == pygame.KEYUP:
                    if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                        player1.x_change = 0
                    if event.key == pygame.K_UP or event.key == pygame.K_DOWN:
                        player1.y_change = 0
            else:
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_a:
                        player2.x_change = -4
                    if event.key == pygame.K_d:
                        player2.x_change = 4
                    if event.key == pygame.K_w:
                        player2.y_change = -4
                    if event.key == pygame.K_s:
                        player2.y_change = 4
                if event.type == pygame.KEYUP:
                    if event.key == pygame.K_a or event.key == pygame.K_d:
                        player2.x_change = 0
                    if event.key == pygame.K_w or event.key == pygame.K_s:
                        player2.y_change = 0
        if player1.active:
            player1.update()
            player1.limit()
        else:
            player2.update()
            player2.limit()

        # game score
        if player1.active:
            for enemy in moving_enemies:
                if enemy.y >= player1.y - 50 and not enemy.p1pass:
                    p1_score += 20
                    enemy.p1pass = True
            for enemy in static_enemies:
                if enemy.y >= player1.y - 50 and not enemy.p1pass:
                    p1_score += 10
                    enemy.p1pass = True
            player1.score = p1_score
        if player2.active:
            for enemy in moving_enemies:
                if enemy.y <= player2.y + 50 and not enemy.p2pass:
                    p2_score += 20
                    enemy.p2pass = True
            for enemy in static_enemies:
                if enemy.y <= player2.y + 50 and not enemy.p2pass:
                    p2_score += 10
                    enemy.p2pass = True
            player2.score = p2_score
        # enemy movement
        if player1.active:
            show_enemy_moving(moving_enemies, 5*pow(p1_level, 0.5))
            show_enemy_static(static_enemies)
        else:
            show_enemy_moving(moving_enemies, 5*pow(p2_level, 0.5))
            show_enemy_static(static_enemies)

        # collision
        if player1.active:
            for enemy in moving_enemies:
                if is_collision(enemy, player1):
                    show_ghost(player1)
                    print("Player 1 " + hit)
                    player1.active = False
                    player2.active = True
                    player1.x = 186
                    player1.y = 876
                    break
            for enemy in static_enemies:
                if is_collision(enemy, player1):
                    show_ghost(player1)
                    print("Player 1 " + hit)
                    player1.active = False
                    player2.active = True
                    player1.x = 186
                    player1.y = 876
                    break
        else:
            for enemy in moving_enemies:
                if is_collision(enemy, player2):
                    show_ghost(player2)
                    print("Player 2 " + hit)
                    player2.active = False
                    player1.active = False
                    player2.x = 186
                    player2.y = 0
                    break
            for enemy in static_enemies:
                if is_collision(enemy, player2):
                    show_ghost(player2)
                    print("Player 2 " + hit)
                    player2.active = False
                    player1.active = False
                    player2.x = 186
                    player2.y = 0
                    break
        # Winning Condition
        if player1.active:
            if player1.y <= 65:
                player1.success = True
                p1_level += 1
                print("Player 1 " + success)
                player1.active = False
                player2.active = True
                end_time = pygame.time.get_ticks() // 1000
                p1_score += int(pow((end_time - start_time), -1) * 100)
                start_time = pygame.time.get_ticks() // 1000
        else:
            if player2.y >= 870:
                player2.success = True
                p2_level += 1
                print("Player 2 " + success)
                player2.active = False
                end_time = pygame.time.get_ticks() // 1000  # you get the bonus only for completing
                p2_score += int(pow((end_time - start_time), -1) * 100)
                start_time = pygame.time.get_ticks() // 1000
        # displaying Players
        if player1.active:
            player1.display()
        player2.display()
        if player1.active:
            score1()
        else:
            score2()
        if not player1.active and not player2.active:
            running = False
        pygame.display.update()
    return p1_level, p2_level


setup()

