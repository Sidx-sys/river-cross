# River Cross
A simple Game Made using the **pygame**, where the objective of each player is to reach the other end 
of the River.
### The Game has the following components:
1. Player 1 -> **Santa Claus**
2. Player 2 -> **Pirate**
3. Moving Enemy -> **Ships**
4. Static Enemy -> **Kill Zone**

### Rules followed in the game
1. **20 points** are given for passing an moving enemy and **10 points** ar  e given for passing an static enemy.
2. If a player reaches the other end, it gets an added **time bonus**
3. The first player to *complete 3 rounds* wins the game
4. If both the players complete the 3 rounds together, then the total points is used as a tiebreaker

### Player Controls
1. Santa can be moved by up, down, left, right arrow keys
2. Pirate can be moved by w, a, s, d keys 




 
